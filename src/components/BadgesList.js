import React from "react";

import twitterIcon from "../images/twitter-icon.svg";

class BadgesList extends React.Component {
	render() {
		return (
			<ul className="list-unstyled">
				{this.props.badges.map((person, index) => {
					return (
						<li className="Badges__element mb-4" key={person.id}>
							<div className="container-fluid">
								<div className="row pt-3 pb-3">
									<div className="col-2">
										<img className="avatar" src={person.avatarUrl} alt="Avatar"/>
									</div>
									<div className="col-9 ml-3">
										<p className="social-text">
											<b>{person.firstName} {person.lastName}</b><br/>
											<a href={"//twitter.com/" + person.twitter} target="_blank" rel="noopener noreferrer">
												<img className="social" src={twitterIcon} alt="Twitter"/>
												@{person.twitter}
											</a><br/>
											{person.jobTitle}
										</p>
									</div>
								</div>
							</div>
						</li>
					)
				})}
			</ul>
		);
	}
}

export default BadgesList;

import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./global.css";

// import BadgeNew from "./pages/BadgeNew.js";
import Badges from "./pages/Badges.js";

const container = document.getElementById("app");

// const nombre = "Hector"
// const sum = () => 3 + 3
// const element = React.createElement("h1", {}, `Hola soy ${nombre}`)
// const jsx = <h1>Hola, soy {/* EXPRESSIONS GO HERE */}</h1>
// const jsx = <div>
// 		<h1>Hola, soy {nombre}</h1>
// 		<p>Soy ingeniero Front End</p>
// 	</div>
// const element = React.createElement(
// 	"div",
// 	{},
// 	React.createElement("h1", {}, `Hola, soy ${nombre}`),
// 	React.createElement("p", {}, `Soy ingeniero Front End`),
// )
// ReactDOM.render(que, donde)
// ReactDOM.render(jsx, container)

// ReactDOM.render(<Badge firstName="Snoopy"
// 	lastName="Avalos"
// 	jobTitle="Kickass Senior Dev"
// 	twitter="hecaval97"
// 	avatarUrl="//gravatar.com/avatar?d=identicon" />, container)

// ReactDOM.render(<BadgeNew />, container);
ReactDOM.render(<Badges />, container);
